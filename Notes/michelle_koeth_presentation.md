# TinyML(Machine Learning) WITH ESP32 Presentation
### Michelle Koeth

## Edge Computing
- "Computing that acts on data at the source"
- Collect and Process data at the source
- Higher quality
- Less or no data going out so the data stays private and secure
- Specialized or customized hardware
- The user has ultimate control
- Ex: Raspberry Pi 4 Model B - $55, Adafruit Feather breakout board - $20(much cheaper)
- Look up "Arduino"

## TINYML

- Machine learning on the edge
- Low power compsumption so can be used for long periods of time
- Inferencing - standalone or in a client-server config
- Relatively new technology
- cam one is $8, mic one is $20, accelerometer one is $48
- these boards can have many capabilites such as identifying if youre human for very cheap, this oppurtunity helped Ms.Koeth get into TinyML
- "Use case" - what youre going to do with the technology available

## "Bully Birds" & Indentifying Bird Types
- Machine learner is trained to recognize and diffrenciate birds by looking at photos on the internet or datasets from universities(Cornell) or companies(CalTech)
- Arthur Samuel quote about computers teaching or programing themselves(ties into how AI's learn)
- Rule based vs machine learning
- Rule based - distinguish a classroom vs an alarm(hard to come up with features), is it above 10 dB?, rules put in and come out binary
- Fuzzy logic - probabalistic
- Convolution - fundemental mathematical process, classification neural networks, a filter (kernel) is used to add values of a pixel within an image to its neighboring pixels resulting in feature maps
- Input an image of a bird, last output a classifaction, comes out as a percentage(hers shows the probability of it being every bird type in her dataset)
- Quantization(efficient but lossy, good for less extreme situations), and pruning(finds key features, removes synapses and neurons)
- Edge impulse - the platform, very new cutting edge tech, simpler ways for user interface
- White paper(?)

- Development Cycle - idea, research, data collection, architecting & training(edge impulse), deployment, field test
- Future Development - upgrades that can be done to increase the capability of the board
