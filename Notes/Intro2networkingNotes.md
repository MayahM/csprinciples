# Intro to Networking: Chapter 1 Notes

## Communicating at a Distance

 In the 1900s not every pair of connected telphones could communicate at the same time. Each person with a telephone had a human "operator" to connect and disconnect two telephones when they called each other.

* Later, tech companies had wires run through central offices instead to connecting a central office to someones home. For calls with a far away distance, multiple central offices could have been used for proper connection.

## Computers Communicate Differently

* Short message: to check if an- other computer is available. Medium message: a single picture or a long email message. Long message: a whole movie or a piece of software to install that might take minutes or even hours to download.

* The longer the distance between two computers were, the length of the leased lines were longer and much more expensive.

* Because each tech company had their own way of using telephone wires, it was difficult to connect computers of different brands.

## Early Wide Area Store-and-Forward Networks

* Since it was expensive to send data to another computer that was far away, computers would forward it to a closer computer, they would forward to another computer and so on until the message reached the computer that the intial computerwanted to send data to.

* This was called hopping 

## Packets and Routers

* Packets: a way of breaking down data into multiple fragments making it easier to send them individually in little "packets."

* Later, computer called IMPs were created for the special process of moving packets. Eventually they were officially changed to be called routers as their purpose was to route packets.

## Addressing and Packets

* LAN: “Local Area Network”, connected multiple computers with physical wiring. WAN: “Wide Area Network”, a router connected to a LAN.

* An address was made for each packet so that routers could find the best path. 

## Putting it all Together

* The "Internet" is made up of all the components previously discussed. 

# Glossary

* address: A number that is assigned to a computer so that messages can be routed to the computer.
* hop: A single physical network connection. A packet on the In- ternet will typically make several “hops” to get from its source computer to its destination.
* LAN: Local Area Network. A network covering an area that is limited by the ability for an organization to run wires or the power of a radio transmitter.
* leased line: An “always up” connection that an organization leased from a telephone company or other utility to send data across longer distances.
* operator (telephone): A person who works for a telephone com-
pany and helps people make telephone calls.
* packet: A limited-size fragment of a large message. Large messages or files are split into many packets and sent across the Internet. The typical maximum packet size is between 1000 and 3000 characters.
* router: A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the best outbound link to speed the packet to its destination.
* store-and-forward network: A network where data is sent from one computer to another with the message being stored for relatively long periods of time in an intermediate computer waiting for an outbound network connection to become available.
* WAN: Wide Area Network. A network that covers longer distances, up to sending data completely around the world. A WAN is generally constructed using communication links owned and managed by a number of different organizations.

## Questions

* 1. What did early telephone operators do?
 * b) Connected pairs of wires to allow people to talk
* 2. What is a leased line?
 * d) An “always on” telephone connection
* 3. How long might a message be stored in an intermediate com- puter for a store-and-forward network?
 * d) possibly as long as several hours
* 4. What is a packet?
 * c) A portion of a larger message that is sent across a network
* 5. Which of these is most like a router?
 * a) A mail sorting facility
* 6. What was the name given to early network routers?
 * d) Interface Message Processors
* 7. In addition to breaking large messages into smaller seg- ments to be sent, what else was needed to properly route each message segment?
 * a) A source and destination address on each message segment
* 8. Why is it virtually free to send messages around the world using the Internet?
 * c) Because so many people share all the resources
# Chapter 2: Network Architecture

## Internet layers

### Application:

* All layers work together to send data across a network
* 2 halves of the application layer are the server and the client
* Server waits for incoming networking connections
* Client runs on the source computer(like browsing the web using software like Firefox, Chrome, or Internet Explorer, you are running a “web client”

### Transport: 

* If the packets were to arrive at the destination computer out of order, the packet still an offset to know where in the message it "fits"
* The intial sending computer keeps a copy of the message until the packets reach their destination
* Window size: amount of data that the source computer sends before wait- ing for an acknowledgement

### Internetwork: 

* When a packet crosses the first link it goes in a router
* Since each router has to handle finding the desination of billions of destination computers, it does its best to the best path to get the packet closer to its destination
* As the packet gets closer to its destination, the routers will gain a better idea of exactly where your packet needs to go
* Routers can message each other about any kind of traffic delay 

###  Link:

* The connection between a computer to its LAN
* Link layer technologies are often shared amongst multiple computers at the same location
* First problem: how to encode and send data
* Second problem: how to cooperate with other computers that might want to send data at the same time 
* CSMA: "When your computer wants to send data, it first listens to see if another computer is already sending data on the network (Carrier Sense)" 
* In case of collision between two computers data, they will stop transmitting, wait, and retry the transmission

### Glossary

* client: In a networked application, the client application is the one that requests services or initiates connections.
* fiber optic: A data transmission technology that encodes data using light and sends the light down a very long strand of thin glass or plastic. Fiber optic connections are fast and can cover very long distances.
* offset: The relative position of a packet within an overall message or stream of data.
* server: In a networked application, the server application is the one that responds to requests for services or waits for incoming
connections.
* window size: The amount of data that the sending computer is allowed to send before waiting for an acknowledgement.

### 2.7 Questions
1. C
2. A
3. B
4. A
5. B
6. A
7. D
8. A
9. C

# Chapter 3: Link Layer

## Sharing the Air

* Called the lowest layer because it is closest to the physical net- work media
* Ways of transmission: wire, fiber optic or a radio signal
* A computing device using the internet can only reach as about 300 ft
* Computers are sble to "hear" any packet sent so that they can tell which ones go through them or to ignore them
* This can cause an issue with protectinfg bank account numbers and other passwords
* every wifi radio has its own serial number
* you can view it in your settings, it will be shown in this format: 0f:2a:b3:1f:b3:1a
* that was 48 bits
* mac address: "from" and "to"

## Courtesy and Coordination

* Carrier sense explained in earlier chapter notes, a way of avoiding collisions and possibility of lost data
* Collision Detection: how a computer tell if it has recieved what was supposed to be sent
* Formal name: “Carrier Sense Multiple Access with Collision Detection”

## Coordination in Other Link Layers

* Stations cannot start a transmission without a token: indicates when each station is given the opportunity to transmit data
* Similar to humans only being able to speak in a conversation when a ball is passed to them
* There are both pro and cons to the CSMA/CD and the token-style network approach

## Glossary

* Base station: Another word for the first router that handles your packets as they are forwarded to the Internet.
* Broadcast: Sending a packet in a way that all the stations con- nected to a local area network will receive the packet.
* Gateway: A router that connects a local area network to a wider area network such as the Internet. Computers that want to send data outside the local network must send their packets to the gateway for forwarding.
* MAC Address: An address that is assigned to a piece of network hardware when the device is manufactured.
* Token: A technique to allow many computers to share the same physical media without collisions. Each computer must wait until it has received the token before it can send data.

## Questions

1. A
2. C
3. A
4. B
5. A
6. D
7. D

# Chapter 4: Interworking Layer (IP)

A packet will need to use different types of transportaion to reach destinations as far as another country. Smiliar to a bus, train, or a plane, a packets uses different link layers like WiFi, Ethernet, fiber optic and satellite. Routers are like train stations. Hops are the series of links along a route.

## IP Addresses

* Using a link layer address in ineffcient because there is no relationship between a link layer address and the location where that computer is connected to the network. With billions of computers on the network it would take a while to make routing decisions using a link layer address.
* IPv4: four numbers seperated by dots(212.78.1.25)
* IPv6: 2001:0db8:85a3:0042:1000:8a2e:0370:7334
* Network Number: 212.78, Host Identifier: 1.25
* Using a 2-part network address is helpful for routers so they can keep track of less network numbers

## How Routers Determine the Routes

* When a router doesn't know all the the routes a packet can take, it will ask its "neighbor" routers for that information
* A routers "Routing table" helps them create a map of the possible paths its packets can take

## When Things Get Worse and Better

* Possible problems: one of the outbound links failing, cable accidentally unplugged
* For a failed outbound link, the router will discard the possible paths using that link and find new routes not including the broken link
* Routers are always trying to improve their knowledge on the paths their packets can take

## Determining Your Route

* no place in the Internet that knows in advance the route your packets will take from your computer to a particular destination
* Routers only know link by link not the full path
* Infinite packet vortex: packets with a network number prefix, if a router gets confused the packets could be stuck in a loop of link forever

## Getting a IP Address

* Dynamic Host Configuration Protocol: how a computer findsit ip address depending on its location
* When reusing a loaned IP address goes wrong your computer could end up with the same ip address as a different computer
* When a computer picks its own random ip address, it doesn't have access to a gate way and therefore can't send any packets

## A Different Kind of Address Reuse

* some prefixes in IP address are special
* Non-routable IP addresses start with “192.168.”
* they can't send data across a global network only a local network

## Global IP Address Allocation

* As the number of IP addresses increased, there were less network numbers that were available
* IPv6 addressses are 128-bit, they were made after the 32-bit IPv4 addresses

## Glossary

* Core router: A router that is forwarding traffic within the core of
the Internet.
* DHCP: Dynamic Host Configuration Protocol. DHCP is how a portable computer gets an IP address when it is moved to a new location.
* edge router: A router which provides a connection between a
local network and the Internet. Equivalent to “gateway”.
* Host Identifier: The portion of an IP address that is used to identify a computer within a local area network.
* IP Address: A globally assigned address that is assigned to a computer so that it can communicate with other computers that have IP addresses and are connected to the Internet. To simplify routing in the core of the Internet IP addresses are broken into Network Numbers and Host Identifiers. An example IP address might be “212.78.1.25”.
* NAT: Network Address Translation. This technique allows a single global IP address to be shared by many computers on a single local area network.
Network Number: The portion of an IP address that is used to identify which local network the computer is connected to.
* Packet vortex: An error situation where a packet gets into an infinite loop because of errors in routing tables.
* RIR: Regional Internet Registry. The five RIRs roughly correspond to the continents of the world and allocate IP address for the ma- jor geographical areas of the world.
* Routing tables: Information maintained by each router that keeps track of which outbound link should be used for each network number.
* Time To Live (TTL): A number that is stored in every packet that is reduced by one as the packet passes through each router. When the TTL reaches zero, the packet is discarded.
Traceroute: A command that is available on many Linux/UNIX systems that attempts to map the path taken by a packet as it moves from its source to its destination. May be called “tracert” on Windows systems.
Two-connected network: A situation where there is at least two possible paths between any pair of nodes in a network. A two- connected network can lose any single link without losing overall connectivity.

## Questions

1. A
2. C
3. B
4. C
5. A
6. C
7. B
8. D
9. D
10. A
11. D
12. B
13. B
14. A
15. A
16. B
17. A
18. D
19. A
20. C
21. C(?)
22. C

## Binary Numeber: inet 10011110.111011.11100001.1110011 

# Chapter 5: The Domain Name System

* Domain Name System: lets you access websites by their domain name like www.khanacademy.org, it's also convenient because you don't have memorize a bunch of numbers because it will have a symbolic name
## Allocating Domain Names

* ICANN chooses the TLDs: .com, edu, .org. These make a domain's name accessable
* ccTLDs: domain names assigned for a specfic country
* Top level domains can have subdomains made by other organizations and indiviuals. For .org and .com you have to pay to add these to your domain name

## Reading Domain Names

* For a IP address you read broad to narrow/ left to right(network number to host indentifier)
* For a domain name you read narrow to broad/ right left(general part of domain name to particular institution or organization)

## Glossary

DNS: Domain Name System. A system of protocols and servers that allow networked applications to look up domain names and retrieve the corresponding IP address for the domain name.
* Domain name: A name that is assigned within a top-level do- main. For example, khanacademy.org is a domain that is assigned within the “.org” top-level domain.
* ICANN: International Corporation for Assigned Network Names and Numbers. Assigns and manages the top-level domains for the Internet.
* Registrar: A company that can register, sell, and host domain names.
* Subdomain: A name that is created “below” a domain name. For example, “umich.edu” is a domain name and both “www.umich.edu” and “mail.umich.edu” are subdomains within “umich.edu”.
* TLD: Top Level Domain. The rightmost portion of the domain name. Example TLDs include “.com”, “.org”, and “.ru”. Recently, new top-level domains like “.club” and “.help” were added.

## Questions

1. What does the Domain Name System accomplish?
A. It allows network-connected computers to use a textual name for a computer and look up its IP address

2. 2. What organization assigns top-level domains like “.com”, “.org”, and “.club”?
C. ICANN - International Corporation for Assigned Network
Names and Numbers

3. Which of these is a domain address?
C. www.khanacademy.org

4. Which of these is not something a domain owner can do with their domain?
C. Creat new top-level domains

# Chapter 6: Transport Layer

* Above the Internetworking, the Transport layer cannot guarentee the delivery of the packets it sends, it is an imperfect layer

* This layer helps with ressambling a message made of multiple packets

## Packet Headers

* Headers of a packet: Link(From|To), IP(From|To|TTL), TCP(Port|Offset)
* a packet gets a new link header after each hop it makes
* IP and TCP headers stay with the packet throughout the journey
* IP contains the IP addresses and the Time To Live protocol
* TCP organizes the packets so they are sent in order of the message

## Packet Reassembly and Retransmission

* The destination computer places the packets in order based on the offset positions attached to each packet
* The sending computer waits for the destination computer to let it know that the packets were recieved
* The sending computer tracks the amount of time it takes to recieve acknowledgements, depending on that it will how much data it should send at a time
* If a packet gets lost, the sending computer will eventually find out by the window size filling up
* The sending and destination computers communicate by tracking the amount time it takes for a packet or an acknowledgement to come. When a packet is lost, after too much time the destination computer lets the sending one know and then sending computer "backs up" and resends data from the last packet actually delivered

## The Transport Layer In Operation

* The sending computer doesn't discard any data sent until are packets in a message are successfully delivered
* Look at photo in text and paragraphs describing it

## Application Clients and Servers

* Client intiates connection and server responds(terms explained in earlier chapter)

## Server Applications and Ports

* Example server applications: web, video or mail
* web clients needs to connect to a web server running on a remote computer
* a client application needs to know which remote computer it should connect to as well as the application it should interact with on that remote computer
* Types of normal ports for server applications: Telnet (23) - Login, SSH (22) - Secure Login, HTTP (80) - World Wide Web, HTTPS (443) - Secure Web, SMTP (25) - Incoming Mail, IMAP (143/220/993) - Mail Retrieval • POP (109/110) - Mail Retrieval, DNS (53) - Domain Name Resolution • FTP (21) - File Transfer
* If you are doing something like web development you would need a non-standard port like 3000, 8080, or 8888

## Glossary

* Acknowledgement: When the receiving computer sends a no- tification back to the source computer indicating that data has been received.
* Buffering: Temporarily holding on to data that has been sent or received until the computer is sure the data is no longer needed.
* Listen: When a server application is started and ready to accept incoming connections from client applications.
* Port: A way to allow many different server applications to be waiting for incoming connections on a single computer. Each application listens on a different port. Client applications make connections to well-known port numbers to make sure they are talking to the correct server application.

## Questions

1. What is the primary problem the Transport (TCP) layer is supposed to solve?
C. Deal with lost and out-of-order packets
2. What is in the TCP header?
C. Port number and offset
3. Why is “window size” important for the proper functioning of the network?
B. It prevents a fast computer from sending too much data on a slow connection
4. What happens when a sending computer receives an acknowledgement from the receiving computer?
B. The sending computer sends more data up to the window size
5. Which of these detects and takes action when packets are lost?
D. Receiving computer
6. Which of these retains data packets so they can be retransmitted if a packets lost?
A. Sending computer
7. Which of these is most similar to a TCP port?
C. Apartment number
8. Which half of the client/server application must start first?
A. Client
9. What is the port number for the Domain Name System?
C. 53
10. What is the port number for the IMAP mail retrieval protocol?
D. 143
# Chapter 7: Application Layer

The Application layer is where the networked software like web browsers, mail programs, video players, or networked video players operate.

## Client and Server Applications

* Web browser on your computer sends a request to connect to a website
* Your computer finds the domain name corresponding with the IP address
* Over the network connection the computer then requests that data from the server
* When the data is received, the web browser shows it to you
* the web server program is on the other side of this connection and waits until that person's computer makes the connection
* All the lower layers including the Domain Name System work together  like a telephone network for networked applications

## Application Layer Protocols

* Protocol: a rule which describes how an activity should be performed, especially in the field of diplomacy
* The formal name for the protocol between web clients and web servers is called the HyperText Transport Protocol, also known as HTTP, you are retrieving a document using the HTTP when it is at the beginning of a URL
* Used more often and is more succesful because of how simple it is to use

## Exploring the HTTP Protocol

* In 1985 the inernet was created by the NSFNet Project
* In 1969 the precursor to the NSFNet was created, it was called the ARPANET
* The telnet application can be found in operating system like Windows 95 through Windows XP, Macintosh's terminal and Linux
* Port 80 is where the HTTP server application is typically found
* If a user does not know the protocol, the web server will not act kindly and the attempt at connection will most likely end in error
* Send a GET command when trying to connect to the web browser again using telnet
* After sending a proper request the host shows headers or metadata about the document available
* Status code: 200 is good, 404 is bad and 301 is the document you're looking for has been moved

## The IMAP Protocol for Retrieving Mail

* The mail application retrieves mail from the central server and saves it if someones computer is off until you turn on your computer
* IMAP: Internet Message Access Protocol
* RFC: Request For Comment
* Example of how the client and server interact
```
C: A142 SELECT INBOX
S: * 172 EXISTS
S: * 1 RECENT
S: * OK [UNSEEN 12] Message 12 is first unseen
S: * OK [UIDVALIDITY 3857529045] UIDs valid
S: * OK [UIDNEXT 4392] Predicted next UID
S: * FLAGS (\Answered \Flagged \Deleted \Seen \Draft)
S: * OK [PERMANENTFLAGS (\Deleted \Seen \*)] Limited
S: A142 OK [READ-WRITE] SELECT completed
```
* This way of communicating is best for the client and server to understand, not really for humans though

## Flow Control

* While the transport layer ignores where the packets where sent from and their destination, that is what the application focuses on
* When the server starts sending the data through the transport layer, if the window size fill up then the server must wait until it gets an acknowledgement form the Transport layer that it can continue sending data
* Look at book to see a photo example of all layers
* The web browser helps reconstruct based on their offsets, the data is displayed as it is recieved

## Writing Networked Applications

* You can send and recieve data in multiple programming languages
* Port 80 tells the computer that we want to connect to a World Wide Web server using the HTTP

## Glossary

* HTML: HyperText Markup Language. A textual format that marks up text using tags surrounded by less-than and greater-than characters. Example HTML looks like: <p>This is <strong>nice</strong></p>.
* HTTP: HyperText Transport Protocol. An Application layer proto- col that allows web browsers to retrieve web documents from web servers.
* IMAP: Internet Message Access Protocol. A protocol that allows mail clients to log into and retrieve mail from IMAP-enabled mail servers.
* flow control: When a sending computer slows down to make sure that it does not overwhelm either the network or the desti- nation computer. Flow control also causes the sending computer to increase the speed at which data is sent when it is sure that the network and destination computer can handle the faster data rates.
* socket: A software library available in many programming lan- guages that makes creating a network connection and exchang- ing data nearly as easy as opening and reading a file on your computer.
* status code: One aspect of the HTTP protocol that indicates the overall success or failure of a request for a document. The most well-known HTTP status code is “404”, which is how an HTTP server tells an HTTP client (i.e., a browser) that it the requested document could not be found.
* telnet: A simple client application that makes TCP connections to various address/port combinations and allows typed data to be sent across the connection. In the early days of the Internet, tel- net was used to remotely log in to a computer across the network.
* web browser: A client application that you run on your computer to retrieve and display web pages
* web server: An application that deliver (serves up) Web pages

## Questions

1. A
2. B 
3. D
4. D
5. A
6. B
7. D
8. A
9. D
10. D
11. A
12. D
13. D 
14. C
15. D
16. A

# Chapter 8: Secure Tansport Layer

* But as the use of the Internet grew rapidly in the late 1980s and literally exploded when the Web became mainstream in 1994, se- curity and privacy of network traffic became very important prob- lems to solve.
* How to secure network activity: secure network hardware and the best way is to encrypt the data someone is sending and decrypting when it reaches its destination

## Encrypting and Decrypting Data

* Caesar Cipher: how Roman armies sented coded messages, shift each layer down the alphabet to produce a ciphertext

## Two kinds of secrets

* The traditional to encrypt transmissions is using a shared se- cret (a password, a sentence, a number) that only the sending and receiving parties know
* If an unencrypted message is sent over the internet, an attacker could access the message, alter it, and send the modified message to its original destination
* The encryption key is known as the public key 
* The decryption key is known as the privte key

## Secure Sockets Layer (SSL)

* To protect the existing internet protocols or architecture, the optional partial layer placed in between the top two layers was formed
* That layer is known as the Secure Sockets Layer (SSL) or the Transport Layer Security (TLS)
* If an encrypted connection is requested, the data is encrypted before it travels and goes through the layers the same as it would unencrypted

## Encrypting Web Browser Traffic

* http or https to indicates whether the data is encrypted
* Your browser will display a lock symbol by the domain name to show that it is encrypted

## Certificates and Certificate Authorities

* Rogue computers can send you public keys and use that to take your data because when you send back your encrypted data they now have the key to decrypt it
* The certificate authority will send you a public key to make sure you are on a trusted and secure site
## Summary

* Asymmetric key: An approach to encryption where one (public) key is used to encrypt data prior to transmission and a different (private) key is used to decrypt data once it is received.
* Certificate authority: An organization that digitally signs public keys after verifying that the name listed in the public key is actually the person or organization in possession of the public key.
* Ciphertext: A scrambled version of a message that cannot be read without knowing the decryption key and technique.
* Decrypt: The act of transforming a ciphertext message to a plain text message using a secret or key.
* Encrypt: The act of transforming a plain text message to a ciphertext message using a secret or key.
* Plain text: A readable message that is about to be encrypted before being sent.
* Private key: The portion of a key pair that is used to decrypt transmissions.
* Public key: The portion of a key pair that is used to encrypt transmissions.
* Shared secret: An approach to encryption that uses the same key for encryption and decryption.
* SSL: Secure Sockets Layer. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Transport Layer Security (TLS).
* TLS: Transport Layer Security. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Secure Sockets Layer (SSL).

## Questions

1. A
2. B
3. D
4. A
5. D
6. D
7. C
8. A
9. C
10. C
11. C
12. C
13. C
