# Create Performance Task Scoring Guidelines

## Requirement for Maximum Points

### What do I need?
Program Purpose and Function

* Program
* Video demonstration of video
   * Input
   * Program functionality
   * Output
* Written Response
   * Desribes the overall purpose of the program
   * describes input and output demonstrated in the video

Data Abstraction

* Two program code segments
  * How data was stored in collection type
  * Data's purpose for being in collection type
  * If more than two code segments are in written response then only the first two will be evaluated and determine whether the point is earned

Managing Complexity

* A code segment showing how a list manages complexity in program
* Explains why list is essential to program
 
Procedural Abstraction

* Must have a **student-developed funciton/procedure** containing at least one parameter. Must affect functionalituyy of program in some way
* Must show function being called
* If multiple procedures are made then the first one will determine whther or not the point is earned
* Must have explicit parameters defined in the header of the procedure

Algorithm Implementation

* Must have a **student-developed algorithm** that contains a sequence, selection and an iteration
* In the written response explain algorithm steps in details well enough for someone else recreate it

Testing

* Calls for procedures must be able to pass different arguments that will cause a different part of the program to execute
* Must describe the conditions being tested by each call of the procedure 
* Identify the result of each call

### Terminology

* Collection type: Aggregates elements in a single structure. Some examples include: databases, hash tables, dictionaries, sets, or any other type that aggregates elements in a single
structure.
* Procedure: A procedure is a named group of programming instructions that may have parameters and return values. Procedures are referred to by different names, such as method, function, or constructor, depending on the programming language.
* Parameter: A parameter is an input variable of a procedure. Explicit parameters are defined in the procedure header. Implicit parameters are those that are assigned in anticipation of a call to the procedure. For example, an implicit parameter can be set through interaction with a graphical user interface.

* Sequencing: The application of each step of an algorithm in the order in which the code statements are given. 
* Iteration: Iteration is a repetitive portion of an algorithm. Iteration repeats until a given condition is met or for a specified number of times. The use of recursion is a form of iteration.
* Argument(s): The value(s) of the parameter(s) when a procedure is called.
