# Big Idea 1: Creative Development

## Main ideas

- Collaboration on projects with diverse team members representing a variety
  of backgrounds, experiences, and perspectives can develop a better computing
  product.
- A *development process* should be used to design, code, and implement
  software that includes using feedback, testing, and reflection.
- Software documentation should include the programs requirements, constraints,
  and purpose.
- Software should be adequately tested before it is released.
- *Debugging* is the process of finding and correcting errors in software.


## Vocabulary

- code segment: a piece of code
- collaboration: working with one or more persons to accomplish something
- comments: short written or oral remarks on a topic or subject
- debugging: fixing issues in a computer program. 
- event-driven programming
- incremental development process
- iterative development process
- logic error: a piece of code that still runs but does not output what is expected
- overflow error: a file or something containing more values than its meant or can handle
- program: instructions for a computer to follow 
- program behavior: 
- program input: what the user enters into the computer
- program output: the result of what the input made
- prototype: a product made in its early stages, not ready for market
- requirements: things needed for something to be done
- runtime error: an error during program execution. for example misspelling a variable or function name
- syntax error: something forgotten or typed incorrectly in a code segment
- testing: checking to see if you get the result expected or wanted
- user interface: when a human interacts with a computer


## Computing Innovation

A *computer artifact* is anything created using a computer, including apps,
games, images, videos, audio files, 3D-printed objects, and websites.
*Computing innovations* are innovations which include a computer program as a
core part of its functionality.  GPS and digital maps are examples of
computing innovations that build upon the early non-computer innnovation of
map making.

## Errors

Four types of programming errors need to be understood:

1. Syntax errors: something written incorrectly in a piece of code causing failure of execution
2. Runtime errors: an error during program execution. for example misspelling a variable or function name
3. Logic errors: a code segment that does not produce the expected result but does still output a result
4. Overflow errors: a file or something containing more values than its meant or can handle

Which type of error is this?
```
for i in range(10)
    print(i)
```
It is a syntax error because there is not `:` after the `(10)`.

Which type of error is this?
```
# Add the numbers from 1 to 10 and print the result
total = 0
for num in range(10)
    total += num 
print(total)
```
Also a syntax error because of the same reason a the previous question.

Which type of error is this?
```
nums = [3, 5, 8, 0, 9, 11]
result = 1
for num in nums:
    result = result / num
print(result)
```
This is a runtime error.

## Presentaton Topics

- Creativity and Computing Innovations
- Benefits of Collaboration
- Software Development Process 
- Software Testing
- Types of Software Errors
- Software Documentation
