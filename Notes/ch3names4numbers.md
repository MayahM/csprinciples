# Chapter 3 Names For Numbers

## Varibles

* How a computer connects a name to a value
* Space in computer memory
* Variable dyslexia
   * Switching where the variable and assignment are:
     ``5 = x`` for exemple.
* Example:
   * a score in computer game
   * a number for a contact name
* Assignment
   * Setting a variables value
   * a = 4 
      * a is space in the computer's memory and 4 is associated with it
      * when a is entered in code, it will be substituted by 4
   * a = 7.2
      * Typing this in code will change the value of a, making it 7.2
   * A variable can equal another variable
      * x = 9 and y = x, this means both x and y equal 9
   * Conditions to make a variable
      * Must start with an uppercase letter or lowercase letter or underscore
      * Cannot start with a number
      * Cannot be a python keyword
         * and, if, else, if, for, input, return, not, while, etc.
      * Cannot have spaces in a variable
         However you can use camel casing and underscore in between words
   * ![alt text] (/Users/1013557/Desktop/Screen\ Shot\ 2022-09-15\ at\ 10.19.11\ PM.png)
       * The output will be 4 because the variable result has an assignment of   2 * 2 which will equal 4. The computer “knows” that 2*4=4 so when it prints the value of result it will be 4.

