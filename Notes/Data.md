## Vocabulary

- abstraction: a process of handling complexity by hiding unnecessary information from the user
- analog data: data represented physically
- bias(in computing): computer systems that systematically and unfairly discriminate against certain individuals or groups of individuals in favor of others.
- binary number system: a number system used by computers, base 2 numbers 1 and 0
- bit: the smallest increment of data, 1 or 0
- byte: eight bits
- classifying data: organizing data in relevant categories for better effiency
- cleaning data: the process of finding inaccurate, irrelevant or missing parts in data  
- digital data: data represented electronically
- filtering data: editing data to fit into certain criteria
- information: Data is a collection of facts, while information puts those facts into context
- lossless data compression: restores and adequately reconstructs data after the file is decompressed. This is the type of data compression that is used for text files.
- lossy data compression: a type of data compression where information is lost. It is more efficient from a data view but aspects of your file can be lost.
- metadata: a summary about a piece of data
- overflow error: a file containing more data than it is meant to be able to handle
- patterns in data: parts in sets of data that are repeated or familiar
- round-off or rounding error: Difference between a result between using rounding and using exact math
- scalability: the measure of a system's ability to increase or decrease in performance and cost in response to changes in application
