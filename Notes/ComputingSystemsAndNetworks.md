## Vocabulary

- Bandwidth: range of frequencies for data transmission
- Computing device: a device that can compute, store, retrieve and transfer information
- Computing network: interconnected computing devies that can exchange data and share resources with each other
- Computing system: integrated devices that can input, output, process, and sto
re data and info
- Data stream: ongoing transfre of data
- Distributed computing system: multiple computers working together to solve a problem
- Fault-tolerant: when a system continues operating despite one or more components failing
- Hypertext Transfer Protocol (HTTP): a networking protocol for distributed, collaborative, hypermedia information systems
- Hypertext Transfer Protocol Secure (HTTPS): HTTPS is HTTP with encryption and verification, uses Transport Layer Security(an authentication and security protocol widely implemented in browsers and Web servers)
- Internet Protocol (IP) address: a unique address that identifies a device on the internet or a local network
- Packets: a small segment of a larger message
- Parallel computing system: the study, design, and implementation of algorithms in a way as to make use of multiple processors to solve a problem
- Protocols: an established set of rules that determine how data is transmitted between different devices in the same network
- Redundancy: process of providing multiple paths for traffic, so that data can keep flowing even in the event of a failure
- Router: a device that connects two or more packet-switched networks or subnetworks
