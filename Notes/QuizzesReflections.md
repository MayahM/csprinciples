# Quizzes Reflections

## Quiz 1 Number 3

The figure below shows a circuit composed of two logic gatess. The output of the circuit is true.

- [x] Input A must be true.
- [ ] Input A must be false.
- [ ] Input A can be either true or false.
- [ ] There is no possible value of input A that will cause the circuit tohave the output true.

I had no idea what the answer was so I just chose D. A is the correct answer at least one of the inputs have to be true for it to be true.

## Quiz 2 Number 9

Using a binary search, how many iterations would it take to find the letter w?

str [a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z]

- [ ] 2
- [x] 3
- [ ] 23
- [ ] 24

I was not sure what to do so I chose C. B is the correct answer because you have to find the median of the list, the median of that, and the median of that until you're left with one letter which should be w. You would then count how many times you had to find the median. That is the number of iterations.

## Quiz 3 Number 3

Digital alarm clocks display information and visual indicators to help people wake up on time. Which of the indicators could represent a single bit of information? Select two answers.

- [ ] The current month (1-12)
- [ ] The "AM"/"PM" indicator
- [ ] The current hour (1-12)
- [ ] The temperature unit indicator ("C" or "F")

I understood what the question was asking but I didnt see the "Select two answers" part so I only chose D. B & D are the correct answers because a single is 1 or 0. Two options. B & D both represent two things that can be one or the other. 

## Quiz 4 Number 8

Consider the following procedure called mystery, which is intended to display the number of times a number target appears on a list.
>
> ```
> PROCEDURE mystery (list, target)
>   count <- 0
>   FOR EACH n IN list
>     IF (n = target)
>       count <- count + 1
>     ELSE
>       count <- 0
> DISPLAY (count)
> ```
>
Which of the following best describes the behavior of the procedure? Select two answers.

- [a] The program correctly displays the count if the target is not in the list.
- [ ] The program never correctly displays the correct value for count.   
- [x] The program correctly displays the count if the target appears once in the list and also at the end of the list.
- [ ] The program always correctly displays the correct value for count.

I picked only D because I didn't see that I had to pick two answer and also had no idea. The correct answers are C & D because if the target is not in the list the count goes back to zero and the loop repeats.

