def mlist(n):
    """ 
      >>> numbers = [1, 2, 3, 4, 5]
      >>> mlist(numbers)
      120
    """
    product = 1
    for number in n:
        product = product * number
    return product

# Question 14
def sum_evens(to_num):
    """
      >>> sum_evens(4)
      6
      >>> sum_evens(6)
      12
     """
    sum1 = 0
    numbers = list(range(2, to_num + 1, 2))
    for number in numbers:
        sum1 = sum1 + number
    return sum1

# Question 16

# Question 17
def letter_combiner(letter_list):
    """
     >>> letter_combiner(["H", "i"])
     'Hi'
    """
    temp_string = ""
    for letter in letter_list:
        temp_string = temp_string + letter
    return temp_string

def idrk(m):
    """
     >>> idrk(16)
     64
    """
    c = 0
    x = list(range(1,m + 1))
    for m in x:
        if m % 2 != 0:
            c+=m
    return c

def mayah(start,stop):
    """
     >>> mayah(1,10)
     1814427.5
    """
    pro = 1
    sum1 = 0
    for x in range(start, stop + 1):
        pro *= x
        sum1 += x
    return (pro + sum1) / 2

def whats_this(num_list):
    """
      >>> whats_this([1, 2, 3, 4])
      2.5
      >>> whats_this([1, 2, 3, 4, 5])
      3
      >>> whats_this([9, 8, 5, 11, 2])
      5
      >>> whats_this([9, 8, 10, 2])
      9.0
      >>> whats_this([0, 2, 4, 6, 8, 10, 12])
      6
      >>> whats_this([0, 2, 4, 6, 8, 10, 12, 14])
      7.0
    """
    middle_index = len(num_list) // 2
    if len(num_list) % 2 == 0:
        return (num_list[middle_index - 1] + num_list[middle_index - 1] ) / 2
    return num_list[middle_index]

if __name__ == "__main__":
    import doctest 
    doctest.testmod() 

