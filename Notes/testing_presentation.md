# Software Testing

# What is software testing

Software testing is the process of evaluating and examining software to make sure it does what it is supposed to do. 

# Types of software testing
* Unit tests - low level tests; Consist is testing individual classes/segments
* Integration tests - verify that different services running your application will work together
* Functional tests - verify output of application but doesn’t check the states while it is being tested.
* End-to-end tests - Testing by replicating a user to make sure the user flow works as expected.
* Acceptance testing - verifies that the application meets the business requirements.
* Performance testing - Tests the application to see how it performs under specific workloads, for example, testing the speed, reliability, responsiveness, etc.
* Smoke testing - Quick, basic tests that check the functionality of the application.


# How to test software

There are two methods for testing software: manually and automatically.

- Manual
  - Often used when testing in earlier versions. 
  - Can detect more unique bugs/problems
  - Less efficient than automated testing

- Automatic
  - Often used near the end of the development process
  - Much faster than manual testing
  - Scripts can be hard to write

## Instances when software was not tested

* On September 22, 2016, there was a breach of data in Yahoo. Around 500 million credentials were exposed. 
* In the spring of 2018 there was a software glitch in the F-35 Joint Strike Fighter aircrafts. This caused the planes to incorrectly detect targets. 
* In September 2017, the credit reporting agency Equifax announced that over a hundred million of their consumer records were stolen by hackers. This meant that about 50% of Americans could be in danger of identity theft or worse. Even though the hack took place in May, the company only reported it in September. 

## Sources
[Source 1](https://www.tricentis.com/blog/real-life-examples-of-software-development-failures)
[Source 2](https://www.atlassian.com/continuous-delivery/software-testing/types-of-software-testing) 
[Source 3](https://www.globalapptesting.com/blog/software-testing)
[Source 4](https://www.cigniti.com/blog/37-software-failures-inadequate-software-testing/)






