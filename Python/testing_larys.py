from gasp import *           # So that you can draw things
from random import randint   # Use this to get randint

def place_player():
    global player_x, player_y, player_shape

    player_x = randint(0, 63)
    player_y = randint(0, 48)
    player_shape = Circle((10 * player_x + 5, 10 * player_y + 5), 5, filled=True)


def place_robot():
    global robot_x, robot_y, robot_shape

    robot_x = randint(0, 63)
    robot_y = randint(0, 48)
    robot_shape = Box((10 * robot_x + 5, 10 * robot_y + 5), 10, 10, filled=False)

    
def move_player():
    global player_x, player_y, player_shape

    key = update_when('key_pressed')

    if key == 'd' and player_x < 63:
        player_x += 1
    elif key == 'c':
        if player_x < 63:
            player_x += 1
        if player_y > 0:
            player_y -= 1

    if key == 'w' and player_y < 47:
        player_y += 1
    elif key == 'q':
        if player_y < 47:
            player_y += 1
        if player_x > 0:
            player_x -= 1
    if key == 'a' and player_x > 0:
        player_x -= 1
    elif key == 'z':
        if player_x < 63:
            player_x -= 1
        if player_y > 0:
            player_y -= 1

    if key == 's' and player_y < 47:
        player_y -= 1
    elif key == 'e':
        if player_y < 47:
            player_y += 1
        if player_x > 0:
            player_x +=1

    move_to(player_shape, (10 * player_x + 5, 10 * player_y + 5))

def move_robot():
    global robot_x, robot_y, robot_shape

    if robot_x > player_x:
        robot_x -= 1
    elif robot_x < player_x:
        robot_x += 1

    if robot_y > player_y:
        robot_y -= 1
    elif robot_y < player_y:
        robot_y += 1

    move_to(robot_shape, (10 * robot_x + 5, 10 * robot_y + 5))
def check_collision():
    if player_x == robot_x and player_y == robot_y:
        Text("You’ve been caught!", (320, 240), size=48)
        return True
    else:
        return False


if __name__ == '__main__':
    begin_graphics()            # Create a graphics window
    finished = False

    
    while not finished:
        place_player()
        move_robot()
        finished = check_collision()

    sleep(5)
    end_graphics()  
