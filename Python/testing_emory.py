from gasp import *          # So that you can draw things
from random import randint


begin_graphics()            # Create a graphics window
done = False

class Player:
    pass

class Robot:
    pass

def place_robot():
    global r
    global robot
    robot = Robot()
    robot.x = random.randint(0,63)
    robot.y = random.randint(0,47)
    r = Box((robot.x * 10 + 5, robot.y * 10 +5), 5, 5, filled = True)

def place_player():
    global c
    global player
    global robot
    player = Player()
    player.x = random.randint(0,63)
    player.y = random.randint(0,47)
    while robot.x == player.x and robot.y == player.y:
        player.x = random.randint(0,63)
        player.y = random.randint(0,47)
    c = Circle((player.x * 10 + 5, player.y * 10 +5), 5, filled = True)

def move_robot():
    global r
    global robot
    global player
    if player.x > robot.x:
        robot.x += 1
    elif player.x < robot.x:
        robot.x -= 1
    if player.y > robot.y:
        robot.y += 1
    elif player.y < robot.y:
        robot.y -= 1
    move_to(r, (robot.x * 10 + 5, robot.y * 10 + 5))

def move_player():
    global c
    global player
    key = update_when('key_pressed')
    while key == "r":
        remove_from_screen(c)
        place_player()
        key = update_when('key_pressed')
    if key == "w" and player.y < 47:
        player.y += 1
    elif key == "a" and player.x > 0:
        player.x -= 1
    elif key == "s" and player.y > 0:
        player.y -= 1
    elif key == "d" and player.x < 63:
        player.x += 1
    elif key == "q" and player.x > 0 and player.y < 47:
        player.x -= 1
        player.y += 1
    elif key == "e" and player.x < 63 and player.y < 47:
        player.x += 1
        player.y += 1
    elif key == "z" and player.x > 0 and player.y > 0:
        player.x -= 1
        player.y -= 1
    elif key == "c" and player.x < 63 and player.y > 0:
        player.x += 1
        player.y -= 1
    move_to(c, (player.x * 10 + 5, player.y * 10 + 5)) 
def check_collisions():
    global done
    if player.x == robot.x and player.y == robot.y:
        done = True
        Text("You lost! :(", (320, 240), size = 50)
        sleep(3)

place_robot()
place_player()

while not done:
    move_player()
    move_robot()
    check_collisions()

end_graphics()              # Finished!

