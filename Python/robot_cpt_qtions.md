# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> A mini game where you're a player(circle) running away from robots(outline squares) because if they touch you, you lose.

2. Describes what functionality of the program is demonstrated in the
   video.
> Depending on what key is pressed, the robots will move closer to the player or the player will move to a random spot on the screen.

3. Describes the input and output of the program demonstrated in the
   video.
> The input is the user pressing a key on the keyboard.
> The output is the player or the robots moving

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
> It is an empty list called robots. A new robot is appended(added) to the list after it is shaped and placed on the screen.
> ![Code Segment](codesegment1.png)

2. The second program code segment must show the data in the same list being used, such as creating new data from the existing data or accessing multiple elements in the list, as part of fulfilling the program’s purpose.
> This function confirms whether or not the player and one or more of the robots collided. 
> ![Code Segment 2](codesegment2.png)

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> The list is called "robots". It is first stated on line 35.

2. Describes what the data contained in the list represent in your
   program
> The code segment represents the creation of each robot that will display on the screen.

3. Explains how the selected list manages complexity in your program code by explaining why your program code could not be written, or how it would be written differently, if you did not use the list.
> Without this code segment, it would be much more difficult to read and understand the code.

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
> This function takes two inputs: thing1 represents any item and list_of_things takes a list 
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>
>

4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>
>

