from random import randint

import time
correct = 0
for q in range(10):
    num1= randint(1,10)
    num2= randint(1,10)
    question = f"What is {num1} times {num2}? "
    response= int(input(question))
    answer = num1 * num2
    if answer == response:
        print("That's right - well done.")
        correct+=1
    else:
        print(f"No, I'm afraid the answer is {answer}.")
print(f"I asked you 10 questions. You got {correct} of them right.")
for x in range(5):
    win1 = randint(1,10)
    win2 = randint(11,20)
    win3 = randint(21,30)
    winners = f"{win1}, {win2} and {win3}"
    print(f"And the lucky winners are: {winners}.")
#while True:
 #   t = time.localtime(time.time())
  #  print(f'The time is {t[3]}:{t[4]} and {t[5]}seconds.')
   # time.sleep(1)
color = input("What's your favorite color? ")
if color == "navy blue":
    print("\nOMG, that's my favorite color!")
elif color != "navy blue":
    print(f"\nYour favorite color is {color}.")
num = randint(1,50)
randomNumber = int(input("\nEnter a number between 0 and 50. "))
while num != randomNumber:
    print("Your " + str(num-randomNumber) + " off.")
    randomNumber = int(input("\nEnter a number between 0 and 50. "))
print(f"\nYou're right, {num} is the correct number.")
num_10 = randint(1,10)
c = 0
guessnumber = int(input("\nEnter a number between 1 and 10. "))
while num_10 != guessnumber:
    print("That's wrong try again.")
    c+=1
    guessnumber = int(input("\nEnter a number between 1 and 10. "))
print(f"That's correct, the number is {num_10}. This took you {c} tries.")
print("Pull out a calculator, what is the distance in kilometers from Earth to the Moon?")
distance_m = 238855
mile1_km = 1.60934
print(f"The distance in miles from the Earth to the Moon is {distance_m}.")
print(f"One mile is equal to {mile1_km} kilometers.")
x = float(input("Please enter the distance from the Earth to the Moon in kilometers. " ))
distance_km = distance_m * mile1_km
c=1
if x != distance_km:
    while x != distance_km:
        print("I'm afraid that's wrong, try again.")
        print("You are " + str(distance_km - x) + " off.")
        x = float(input("Please enter the distance from the Earth to the Moon in kilometers. "))
        c+=1
else:
    print(f"You are correct! The distance in kilometers is {distance_km}.")
print(f"Good job, this took you {c} tries.")

