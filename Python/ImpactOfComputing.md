# Big Idea 5: Impact of Computing 


## Main ideas

- New technologies can have both beneficial and unintended harmful effects,
  including the presence of bias.
- Citizen scientists are able to participate in identification and problem
  solving.
- The digital divide keeps some people from participating in global and local
  issues and events.
- Licensing of people’s work and providing attribution are essential.
- We need to be aware of and protect information about ourselves online.
- Public key encryption and multifactor authentication are used to protect
  sensitive information.
- Malware is software designed to damage your files or capture your sensitive
  data, such as passwords or confidential information.


## Vocabulary

- **Asymmetric Ciphers**: Ciphers for public and private keys. One key is for message encrypting and the other decrypting.
- **Authentication**: The process of determining whether someone or something is, in fact, who or what it says it is.
- **Bias**: Prejudice in favor of or against one thing, person, or group compared with another.
- **Certificate Authority (CA)**: An official document that proves your work is genuine and authentic.
- **Citizen Science**: Public voluntarily helping to conduct scientific research.
- **Creative Commons Licensing**: A type of public copyright license. It allows the free distribution of an otherwise copyrighted work.
- **Crowdsourcing**: Obtaining information from a group of people.
- **Cybersecurity**: the state of being protected against the criminal or unauthorized use of electronic data.
- **Data Mining**: The process of uncovering patterns and other valuable information from large data sets.
- **Decryption**: A process that transforms encrypted information into its original format. 
- **Digital Divide**: The gap between people who have easy access to computers and those who do not.
- **Encryption**: A way of scrambling data so that people can't easily access private things on your mobile device.
- **Intellectual property (IP)**: A category of property for your intangible assets.
- **Keylogging**: A form of spyware where a keyboards activies are tracked. The users are unaware their actions are being tracked.
- **Malware**: Also known as malicious software, is a type of software with malicios intent. Malware can take many form such as viruses, worms, trojan horses, ransomware and spyware.
- **Multifactor Authentication**: Proving you are who you say who are on a computer through multiple ways. 
- **Open access**: Free access to information and unrestricted use of online resources.
- **Open source**: Code that is publicly accessible. For example the programming languages Python and JavaScript.
-**Free Software**: A type of software that respects a user's freedom.
- **FOSS**: A combination of free software and open soure software.
- **PII (Personally Identifiable information)**: Any representation of information that permits the identity of an individual to whom the information applies to be reasonably inferred by either direct or indirect means.
- **Phishing**: A technique for attempting to aquire sensitive data through fraudulent solicitation.  
- **Plagiarism**: The practice of taking someone else's work or ideas and passing them off as one's own.
- **Public Key Encryption**: Encrypted data that can be accessed through a public key.
- **Rogue Access Point**: An electronic device unsanctioned by an administrator however operating on the network anyway.
- **Targeted Marketing**: Raising awareness of a product to a specific audience.
- **Virus**: A program capable of duplicating itself and then corrupting or destroying a system or data.

