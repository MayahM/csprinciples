print("Modular Division: ")
print("A division type that finds the remainder instead of a decimal.")
print(f"16 % 3 gives us {16%3} and 2 % 7 gives us {2%7}.")
