1. How is the APCSP exam scored?
* The exam is scored from 1-5, with 5 being the best score. There are two parts to the exam: the multiple choice section and the create performance task. The multiple choice section counts for 70% of your grade and the create perfomance task is 30%. https://www.bestcolleges.com/computer-science/ap-computer-science-principles/
   * How many of the 70 multiple choice questions do I need to get correct to earn a 5, 4 or 3 on an exam?
      * I do not know the exact number of questions that are corret are needed. However, I found that the cutoff for a 5 is 75% and a 4 is 75%. Recieving a 3 means qualified, a 4 is well qualified and a 5 is extremely qualified.
   * What is the Create Performance Task and how is it scored?
      * It is part of the exam that counts for 30% of your grade. The task is to create a program before the due date which is May 1st. You will get a minimum of 12 of in class time to work on your program. Your program must be submitted using the AP Digital Portfolio. Your portfolio must contain a video of your program running, written responses on the development process and your code. The task is scored by AP Reader. They are trained educators in the College Board.
2. On which of the Big Ideas did I score best?
* I scored best on Big Idea 1.
   * On which do I need the most improvement?
      *  I need improvement on Big Idea 4.
3. What online resources are available to help me prepare for the exam in each of the Big Idea areas?
* I can look at https://apcentral.collegeboard.org/courses/ap-computer-science-principles/exam to look at Create Performance Task examples.

