# Who or What Started World War 2?

## Question 1: Which cause of WWII had the most significant impact on the outbreak of the war? Why?

xxx

## Question 2: Which battle in the European Theater played the greatest role in the outcome of the war? Why?

![europe1](https://www.thoughtco.com/thmb/I1btD0f1MtS7fM-S3UIQTPS6Fho=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/c051107378bbbe284b75100dfc360088-5933c54c3df78c08aba52512.jpg)

On August 23, 1942 was the start of one of the most important battles that took place in the European Theatre. Known as the Battle of Stalingrad it was a battle between former allies Adolf Hitler and Joseph Stalin, Germany and Russia. The alliance between the two leaders broke when Hitler made a surprise attack on the Soviet Union in June, 1941. This caused Russia to join the Allied Powers so they could defeat Germany. 

![europe2](https://www.historyhit.com/app/uploads/2020/07/pavlovs-house-stalingrad-1.jpg)

The Battle of Stalingrad occurred in Stalingrad, Russia because of the importance of its name. It has “Stalin” in it, so both countries found the city important for its symbolism. Hitler thought taking over Stalingrad would be great for propaganda.

## Question 3: Which battle in the Pacific Theater played the greatest role in the outcome of the war? Why?

![Pacific Theatre1](https://www.nationalww2museum.org/sites/default/files/styles/wide_medium/public/2017-06/battle_of_midway-002.jpg?h=9f3afbb3)

On June 3rd, 1942, there was a turning point in the Pacific theater. This turning point was the battle of midway. Decoding Japanese code, the Americans learned Japan was planning to deplete their naval forces at Pearl Harbor in a second attack. Using fake messages, the US confirmed the area of the Japanese fleet and launched an attack. Battle raged on for 3 days, leading to a US victory but casualties in the thousands. After their loss at Midway, Japanese naval forces were weakened, loosening their chokehold on the Pacific islands and giving the allies an advantage. This was the point when the war in the Pacific theater took a turn in favor of the allies, allowing them to win the war.      

![Pacific Theatre2](https://images.nationalgeographic.org/image/upload/t_edhub_resource_key_image/v1638889096/EducationHub/photos/mikuma-at-midway.jpg)

## Question 4: Which components of WWII are essential to classifying this event as "total war"? Why?

![total war](https://www.nationalww2museum.org/sites/default/files/styles/wide_medium/public/2020-06/LL-066%20-%20Kristen%20D.%20Burton.jpg)

Total war is defined as a war where the sides are willing to make any sacrifice to obtain victory. This can be through human resources, the economy, and even civilian lives. In WW2 total war can be seen during and after the war when once gorgeous cities became ruins and supply of material was near non-existent after all of it was put towards the war effort. While WW2 left many countries in shambles, many others benefited. Some examples of this can be seen in Latin American countries who made lots of profit exporting supplies to the other countries, aiding with the war effort. 


## Question 5: How did Germany's access to land and resources precipitate this war?

xxx

## Question 6: What role did some of the mistakes from WWI contribute to WWII? Why?

xxx

## Question 7: What role did the Holocaust play in the war? Why should we study and remember this event in history?

The Holocaust was a major part of the Nazi plan. In the Nazi Party Platform, 7 out of the 25 statements have the purpose of revoking Jewish people’s citizenship or taking away their inalienable rights.

![Holocaust1](https://media.iwm.org.uk/ciim5/37/130/large_000000.jpg)

In the autobiography Night by Eliezer Weisel, near the end of his story he talks about the Red Army closing in, and how the Nazi’s were rushing to exterminate as many Jews as they could before they officially lost the war. This showed that the Nazi’s main motivation for starting World War 2 was to commit genocide on the Jewish people. This is an important event in history to study and remember because of its effects on the community.

![Holocaust2](https://macleans.ca/wp-content/uploads/2017/08/AUG19_HOLOCAUST_POST.jpg)

Some of these effects were the millions of Jewish deaths, anti-semitism, and the effects on the practice of Judaism and the Jewish people’s belief in their religion. Anti-semitism is still committed today and even after decades, the Jewish population is still under its initial number from before the Holocaust. In Night, Mr.Weisel revealed his conflicts with his belief in god and how surviving the holocaust permanently broke the deep bond they shared before.

## Question 8: Why did Americans allow Japanese internment to happen? Why should we study and remember this event in history?

In the Japanese internment video(US pro) we watched, the impression was given that Japanese citizens’ rights were respected and that the actions taken by the government were lawful. However, almost immediately after the attack on Pearl Harbor, Japanese citizens were unfairly arrested by the FBI and their houses were later searched illegally without a warrant. 

On February 19, 1942, President Franklin D. Roosevelt established internment camps specifically for Japanese citizens when he signed the document known as Executive Order 9066. About 120,000 Japanese people, most of them American citizens, were forcibly sent to live in internment camps. Other countries had followed their example and deported Japanese citizens to internment camps in the United States. 

![Japanese Internment](https://assets.editorial.aetnd.com/uploads/2009/10/japanese_internment_camps_getty-514877912.jpg)

 When the Japanese citizens entered the internment camps they encountered racist resistance from the residents that were living there. In addition, violence was known to happen with the camps. Japanese citizens were beaten and or killed by guards claiming the prisoners were rioting or trying to escape. 

Executive Order 9066 violated the Fifth Amendment of the US Constitution because in the amendment it states that “nor be deprived of life, liberty, or property, without due process of law; nor shall private property be taken for public use, without just compensation.” The Japanese citizens were forced out of their homes unjustly and without the proper process of the law.

## Question 9:What led to US involvement in WWII? How did the US joining possibly affect the outcome?


![idk](https://media.iwm.org.uk/cantaloupe/iiif/2/35%7C%7C70%7C%7Clarge_000000.jpg/full/712,/0/default.jpg)

On December 7th, 1941, Japan launched an attack on the US that would change history forever. The attack on pearl harbor was the last straw for the US and is what brought them into WW2. The US was one of the greatest powers in WW2 and fought across 4 theaters, emerging victorious across every single one. Had the US never joined the war effort, these outcomes may have been different. The US was involved in many operations and battles that led to allied victory. These include invasions such as D-Day and the Invasion of Sicily, both of which were enormous factors in Allied victory. To add on to US significance, had the US not fought in the Pacific Theater, there’s a good chance that the axis powers would have emerged triumphantly. Overall, without US involvement, WW2 could have had a very different outcome and the world would almost certainly not be the same.

