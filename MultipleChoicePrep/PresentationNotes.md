# Noah, Anupama, Eimi and Rockwell Presentaion

* Know instructions and study big ideas
* Big idea 3 is worth the most and takes up the majority of the 70 questions
* Take practice tests
* Khan Academy is a good place to study, only javascript tests available however
* College board has past questions
* Test guide website has 4 practice tests(find on Noah's repo, definitely look at these) 

This presentation was extremely helpful to me bacause of all the practice test the group found. With their resources their shouldn't be a single question on the test that suprises me.  

## 3 Practice Problems
* You develop a program that lets you use different operations (addition, subtraction, multiplication, division, etc.) on a set of numbers derived from a database. However, the actual test results give a random value, which leads you to suspect that there is a randomizing variable in your code. 

- [x] Rerunning Code
- [ ] Tracing Code
- [ ] Using a code visualizer
- [ ] Using DISPLAY statements at different points of code

* Musicians record their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower than expected. Which of the following could be a possible reason why this difference exists?

- [ ] The file was moved from the original location, causing some information to be lost.
- [ ] The recording was done through the lossless compression technique.
- [ ] The song file was saved with higher bits per second
- [x] The song file was saved with lower bits per second

* A school had a 90% pass rate for students that took the last AP exam. The school wants to use this achievement to advertise its services to families of prospective students. Which of the following methods would be most effective in delivering this information to the families in a summarized manner?

- [ ] Email the prospective families that have middle-school-age children.
- [ ] Create a report based on the student body's overall performance for a marketing pamphlet.
- [x] Post an interactive pie chart about the topics and scores of the passing students on the schools' website.
- [ ] Post the results of the passing students on social media sites.

Source: [Test Guide](https://www.test-guide.com/ap-computer-science-principles-practice-exam.html).


* (A or B) and not (A and B) / (not A and b) or (A and not B)
