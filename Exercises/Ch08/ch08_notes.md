### Chapter 8 - While and For Loop

# While

* A loop that repeats for as long as it is true. Once a loop becomes false, it will stop running. If the loop is always true then it will run infinitely, an infinite loop.

# Logical Expression

* Expressions that are true or false
* `x = 5 (variable and its assignment)` 
* `x == 3 (== is asking whether or not x is equal to 3)`
* `x <= 5 (<= is asking if x is less than or equal to 5)`
* `x != 5 (!= is asking if x is not equal to 5)`

# Infinite Loop Examples

```
name = "Mayah"
while name == "Mayah":
print("Mayah is awesome")
```
*  This will infinitely print "Mayah is awesome" because the variable name will always have the  string "Mayah" as its value. 

```
while 1 != 2:
print("Looping Forever")
```
*  This will always print "Looping Forever" because 1 will never be equal to 2.

# Counting

```
 mayah = 0
while mayah < 15
    print("Mayah is " + str(mayah) + " years old.")
    mayah += 1
print(f" Mayah is almost {mayah} years old.")
``` 
* Because the code is adding 1 to its value each time the loop runs, once the value of mayah reaches 14 it will add 1 once more and quit the loop and execute any code underneath.

# Comparison

```
 for counter in range(1,11):
    print(counter)
```

```
 counter = 1
while counter < 11:
    print(counter)
    counter += 1
``` 
![Compare While and For](https://www.openbookproject.net/books/StudentCSP/_images/compareWhileAndFor.png)
