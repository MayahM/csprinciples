# Chapter 9 Notes - Repeating Step With Strings

Learning Objectives
* Show how the accumulator pattern works for strings.
* Show how to reverse a string.
* Show how to mirror a string.
* Show how to use a while loop to modify a string.

## String

* A collection of letter digits and other characters. 
* Connection
   * For loops step through letters
   * `+` symbol appends strings together
   * Compatible with accumulator pattern
* Example of changing the pattern in the Accumulator pattern
```
Code:

new_string_a = "" # Empty string
new_string_b = "" # Empty string

phrase = "Happy Birthday!" # Variable with string value

for letter in phrase: # For loop
    new_string_a = letter + new_string_a # Accumulates each character going back to the beginning each run of the loop 
    new_string_b = new_string_b + letter # Accumulates each character starting from the beginning 

print("Here's the result of using letter + new_string_a:") # Print function outputs string
print(new_string_a) # Will print the variable phrase backwards
print("Here's the result of using new_string_b + letter:")
print(new_string_b) # Will print the variable phrase forwards

Output:

Here's the result of using letter + new_string_a:
!yadhtriB yppaH
Here's the result of using new_string_b + letter:
Happy Birthday!
```

