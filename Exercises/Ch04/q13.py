total_mins = 270
num_mins = total_mins % 60
num_hours = (total_mins - num_mins) / 60
print(f"{total_mins} is {num_hours} hours and {num_mins} minutes.")
