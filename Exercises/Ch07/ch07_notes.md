# Chapter 7: Computer can Repeat Steps Notes
## Loops
* A loop or iteration is how a computer repeats steps in a program.
* For
   * A type of loop that repeats a statement or sets of a statement. Takes a variable and uses each value in a list one at a time. When writing a for loop you must put a colon after the first line of the loop. This is so that everything underneath will automatically indent itself. You want this to happen so that the loop runs for as long as it's supposed to. If the code underneath wasn't indented then it would only run once. For example: 
`sum = 0 (#line 1)
things_to_add = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] (#line 2)
for number in things_to_add: (#line 3)
    sum = sum + number(#line 4)
print(sum)`(#line 5)
   * The variable number will take on one item from the list "things_to_add" at a time. The variable sum is adding itself to the value that number takes on. It will continue to do this until the variable number has taken on every item in the list. When the loop is complete the code outside the indentation will print. This will print all the items in the list added together: 55. 
## Lists
* A list holds items in order. It is inclosed by brackets:[]. You have used lists in your daily life if you've ever written a grocery list or to do list. Python is a zero based language so if you were to call the first item in a list you would call item 0. 
## The Range Function
* Causes a for loop to run over the sequece of numbers. If you were to enter one positiv number into a for loop with a range function then it would print all the numbers from 0 till the number before it (because Python is a zero based language.) If you were to print 2 number such as (5, 25) it would print 5 to 24. You can also go change what number you go by as you go to the next number. For example if you entered (5,50,5) it would print from 5 to 49 counting by fives. You would see `5 10 15 20 25 30 35 40 45`. 
## Test this code!
* `sum = 0`
* `numbers = range(50,101,2)`

* `for number in numbers:`
* `____sum = sum + number`
* `print(sum)`
